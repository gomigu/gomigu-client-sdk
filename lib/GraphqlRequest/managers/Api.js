import _ from "lodash";
import pluralize from "pluralize";

// import ApiError from "./ApiError";

// import SlackService from "../../../services/SlackService";
// import security from "../../../config/security";

class Api {
	constructor(requestManager) {
		this._requestManager = requestManager;
		// this._slack = new SlackService(security.slackWebhookUri.error);
	}

	_alterName = (name) => {
		this._name = _.capitalize(name);
		this._pluralName = pluralize(this._name);
		this._resource = _.camelCase(name);
		this._resources = pluralize(this._resource);

		return;
	}

	//
	// Singular queries
	//

	findById = async (modelName, { id }, attributes = [ "id" ]) => {
		this._alterName(modelName);

		const query = `
		query Find${ this._name }ById($id: Int) {
			${ this._resource }(id: $id) {
				${ this._generateAttribTree(attributes) }
			}
		}
		`;

		const variables = {
			id,
		};

		return this._execute(this._resource, query, variables);
	}

	findOne = async (modelName, { where, options }, attributes = [ "id" ]) => {
		this._alterName(modelName);

		const query = `
		query Find${ this._name }($where: Json, $options: Json) {
			${ this._resource }(where: $where, options: $options) {
				${ this._generateAttribTree(attributes) }
			}
		}
		`;

		const variables = {
			where,
			options,
		};

		return this._execute(this._resource, query, variables);
	}

	//
	// Plural queries
	//

	findAndCountAll = async (modelName, { where, offset, limit, order, options },
		attributes = [ "id" ]) => {
		this._alterName(modelName);

		const query = `
		query FindAndCountAll${ this._pluralName }
			($where: Json, $offset: Int, $limit: Int, $order: Json, $options: Json) {
			${ this._resources }(action: COUNT, where: $where, offset: $offset,
				limit: $limit, order: $order, options: $options) {
				offset
				limit
				count
				rows {
					${ this._generateAttribTree(attributes) }
				}
			}
		}
		`;

		const variables = {
			where,
			offset,
			limit,
			order,
			options,
		};

		console.log("FindCOunt", query);

		return this._execute(this._resources, query, variables);
	}

	findAll = async (modelName, { where, offset, limit, order, options }, attributes = [ "id" ]) => {
		this._alterName(modelName);

		const query = `
		query FindAll${ this._pluralName }
			($where: Json, $offset: Int, $limit: Int, $order: Json, $options: Json) {
			${ this._resources }(where: $where, offset: $offset,
				limit: $limit, order: $order, options: $options) {
				offset
				limit
				rows {
					${ this._generateAttribTree(attributes) }
				}
			}
		}
		`;

		const variables = {
			where,
			offset,
			limit,
			order,
			options,
		};

		return this._execute(this._resources, query, variables);
	}

	//
	// Singular mutations
	//

	create = async (modelName, { values, options }, attributes = [ "id" ]) => {
		this._alterName(modelName);

		const query = `
		mutation Create${ this._name }($values: Json, $options: Json) {
			${ this._resource }(action: CREATE, values: $values, options: $options) {
				${ this._generateAttribTree(attributes) }
			}
		}
		`;

		const variables = {
			values,
			options,
		};

		return this._execute(this._resource, query, variables);
	}

	findOrCreate = async (modelName, { values, options }, attributes = [ "id" ]) => {
		this._alterName(modelName);

		const query = `
		mutation FindOrCreate{ this._name }($values: Json, $options: Json) {
			${ this._resource }(action: READ, values: $values, options: $options) {
				${ this._generateAttribTree(attributes) }
			}
		}
		`;

		const variables = {
			values,
			options,
		};

		return this._execute(this._resource, query, variables);
	}

	upsert = async (modelName, { values, options }, attributes = [ "id" ]) => {
		this._alterName(modelName);

		const query = `
		mutation UpdateOrInsert{ this._name }($values: Json, $options: Json) {
			${ this._resource }(action: UPSERT, values: $values, options: $options) {
				${ this._generateAttribTree(attributes) }
			}
		}
		`;

		const variables = {
			values,
			options,
		};

		return this._execute(this._resource, query, variables);
	}

	//
	// Plural mutations
	//

	bulkCreate = async (modelName, { values, options }, attributes = [ "id" ]) => {
		this._alterName(modelName);

		const query = `
		mutation BulkCreate${ this._pluralName }($values: Json, $options: Json) {
			${ this._resources }(action: CREATE, values: $values, options: $options) {
				count
				rows {
					${ this._generateAttribTree(attributes) }
				}
			}
		}
		`;

		const variables = {
			values,
			options,
		};

		return this._execute(this._resources, query, variables);
	}

	update = async (modelName, { values, options }, attributes = [ "id" ]) => {
		this._alterName(modelName);

		const r = (options.limit === 1) ? this._resource : this._resources;
		const query = `
		mutation Update${ this._pluralName }($values: Json, $options: Json) {
			${ r }(action: UPDATE, values: $values, options: $options) {
				count
				rows {
					${ this._generateAttribTree(attributes) }
				}
			}
		}
		`;

		const variables = {
			values,
			options,
		};

		return this._execute(r, query, variables);
	}

	destroy = async (modelName, options, attributes = [ "id" ]) => {
		this._alterName(modelName);

		const r = (options.limit === 1) ? this._resource : this._resources;
		const query = `
		mutation Delete${ this._pluralName }($options: Json) {
			${ r }(action: DELETE, options: $options) {
				count
				rows {
					${ this._generateAttribTree(attributes) }
				}
			}
		}
		`;

		const variables = {
			options,
		};

		return this._execute(r, query, variables);
	}

	_generateAttribTree = (attribTree) => {
		return _.transform(attribTree, (r, attrib) => {
			if (!_.isString(attrib)) {
				attrib = `${ attrib[0] } {
						${ this._generateAttribTree(attrib[1]) }
					}`;
			}

			r.push(`
				${ attrib }
			`);
		}, []).join("\n");
	}

	_execute = async (resource, query, variables) => {
		const { data, errors } = await this._requestManager.post("/graphql", { query, variables });

		console.log("Errors", errors);

		if (errors) {
			throw new Error(errors[0]);
		}

		return data[resource];
	}
}

export default Api;
