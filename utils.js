// import { List } from "immutable";
// import moment from "moment";
import _ from "lodash";

import { MsgEvents } from "__src/api/Messaging";
import * as SocketEvents from "__src/lib/SocketIO";
import * as Types from "./types";
// import * as globals from "__src/globals";
// import * as enums from "__src/config/enums";
// import cryptojs from "crypto-js";
// import security from "__src/config/security";

// const { isList } = List;

const listenerDispatchers = {
	[SocketEvents.CONNECT]: Types.CONNECTED,
	[SocketEvents.DISCONNECT]: Types.DISCONNECTED,
	[SocketEvents.CONNECT_ERROR]: Types.CONNECTION_ERROR,
	[SocketEvents.CONNECT_TIMEOUT]: Types.CONNECTION_TIMEOUT,
	[SocketEvents.RECONNECTING]: Types.RECONNECTING,
	[SocketEvents.RECONNECT]: Types.RECONNECTED,
	[SocketEvents.RECONNECT_ERROR]: Types.RECONNECTION_ERROR,
	[SocketEvents.RECONNECT_FAILED]: Types.RECONNECTION_FAILED,

	[MsgEvents.USER_ACTIVE]: Types.USER_ACTIVE,
	[MsgEvents.USER_INACTIVE]: Types.USER_INACTIVE,
	[MsgEvents.USER_ONLINE]: Types.USER_ONLINE,
	[MsgEvents.USER_OFFLINE]: Types.USER_OFFLINE,
	[MsgEvents.USER_BLOCKED]: Types.USER_BLOCKED,
	[MsgEvents.USER_UNBLOCKED]: Types.USER_UNBLOCKED,
	[MsgEvents.USER_UPDATED]: Types.USER_UPDATED,
	[MsgEvents.SENDER_TYPING]: Types.SENDER_TYPING,
	[MsgEvents.SENDER_IDLE]: Types.SENDER_IDLE,
	[MsgEvents.MESSAGE_NEW]: Types.MESSAGE_NEW,
	[MsgEvents.MESSAGE_SEEN]: Types.MESSAGE_SEEN,
	[MsgEvents.CHANNEL_CREATED]: Types.CHANNEL_CREATED,
	[MsgEvents.CHANNEL_UPDATED]: Types.CHANNEL_UPDATED,
	[MsgEvents.CHANNEL_DELETED]: Types.CHANNEL_DELETED,
	[MsgEvents.CHANNEL_INVITED]: Types.CHANNEL_INVITED,
};

export const handleMessagingEvents = (userId, dispatch, loadThreads) => (
	// eslint-disable-next-line max-statements
	async (event, data) => {
		const type = listenerDispatchers[event];
		// const userId = globals.Messaging.getUserId();

		return dispatch({ type, data });
	}
);