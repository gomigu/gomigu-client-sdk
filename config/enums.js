// message types
export const MESSAGE = "MESSAGE";
export const POST = "POST";
export const ARTICLE = "ARTICLE";
export const MESSAGE_REPLY = "MESSAGE_REPLY";
export const SHARE = "SHARE";
export const COMMENT = "COMMENT";
export const COMMENT_REPLY = "COMMENT_REPLY";
export const NOTE = "NOTE";
export const LIVE_STREAM = "LIVE_STREAM";
export const REPORT = "REPORT";

// channel types
export const GROUP = "GROUP";
export const DIRECT = "DIRECT";
export const NEWSFEED = "NEWSFEED";
export const PENDING_MESSAGE_REQUEST = "PENDING_MESSAGE_REQUEST";
export const ACCEPTED_MESSAGE_REQUEST = "ACCEPTED_MESSAGE_REQUEST";
export const REMOVED_MESSAGE_REQUEST = "REMOVED_MESSAGE_REQUEST";

// message privacy

export const SELF = "SELF";
export const FRIENDS = "FRIENDS";
export const PUBLIC = "PUBLIC";

// message rules
export const MAX_MSG_LENGTH = 200;
export const MAX_MSG_LINE = 5;