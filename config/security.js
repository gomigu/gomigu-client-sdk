import * as custom from "__src/config/custom";

const security = {
	authServerConfig: {
		host: custom.host,
		port: custom.port,
		endpoint: custom.endpoint,
		appName: "",
		apiKey: "217d870fc26f79af29fc191f44dea97a97cfc69a0f630f0d5ebaf55e4b306f28",
	},

	msgServerConfig: {
		host: custom.host,
		port: 1337,
		appName: "",
		apiKey: "949f9e84257ec87514f19b9f7ac1d89958f42f6d98b50c0a0bdaf9c87323fd53",
	},
};

export default security;
